package org.bdshadow;

public class UpperCaseService {

    public String getUpperCaseString(String str) {
        return str.toUpperCase();
    }
}
