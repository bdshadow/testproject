package org.bdshadow;

import java.util.Collections;
import java.util.List;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.junit.JUnitStories;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class UpperCaseServiceTest {

    private UpperCaseService upperCaseService;

    @Given("a service")
    public void service() {
        this.upperCaseService = new UpperCaseService();
    }

    @Then("return its upper case")
    public void mustReturnUpperCase() {
        assertThat(this.upperCaseService.getUpperCaseString("word"), equalTo("WORD"));
    }
}
